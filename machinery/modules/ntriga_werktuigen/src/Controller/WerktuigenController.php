<?php
/**
 * @file
 * Contains \Drupal\ntriga_werktuigen\Controller\WerktuigenController.
 */

namespace Drupal\ntriga_werktuigen\Controller;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\HttpFoundation\Response;

class WerktuigenController {

  use StringTranslationTrait;

    public function xml() {

    	$host = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";

    	$xml = '<?xml version="1.0"?>
		<verkoop id="1">';

    	$nids = \Drupal::entityQuery('node')->condition('type','product')->execute();
    	$nodes =  \Drupal\node\Entity\Node::loadMultiple($nids);

    	foreach ($nodes as $node) {
    		$xml .= '<item>
			<ident_id>'.$node->id().'</ident_id>
            <square_id>'.$node->field_categorie->entity->getName().'</square_id>';

			foreach ($node->field_afbeeldingen as $index => $afbeelding) {
    			$xml .= '<image_'.($index+1).'>'.file_create_url($afbeelding->entity->getFileUri()).'</image_'.($index+1).'>';
    		}

    		$content = $node->field_uitvoering->getValue()[0]['value'];
    		$content .= $node->field_tech_specs->getValue()[0]['value'];
    		$content .= $node->field_modellen->getValue()[0]['value'];
    		$content .= $node->field_opties->getValue()[0]['value'];

    		if (isset($node->field_merk->entity) && !$node->field_tweedehands->getValue()[0]['value']) {
    			$xml .= '<brand><![CDATA['.$node->field_merk->entity->getName().']]></brand>';
    		}else{
    			$xml .= '<brand></brand>';
    		}

			$xml .= '
			<model><![CDATA['.$node->getTitle().']]></model>
			<title><![CDATA['.$node->getTitle().']]></title>
			<fuel>-1</fuel>
			<condition>'.($node->field_tweedehands->getValue()[0]['value']?'3':'1').'</condition>
			<description><![CDATA['.$content.']]></description>
			<url><![CDATA['.($host.$node->toUrl()->toString()).']]></url>
            <article_number><![CDATA['.$node->field_referentie->getValue()[0]['value'].']]></article_number>
            <weight></weight>
            <hours></hours>
            <mileage></mileage>
            <hp></hp>
            <kw></kw>
            <kva></kva>
            <volt></volt>
            <lift_capacity></lift_capacity>
            <lift_height></lift_height>
            <mast_height></mast_height>
            <working_height></working_height>
            <traction></traction>
            <lpm></lpm>
            <year_manufacture></year_manufacture>
			<price></price>
			<price_type>8</price_type>
			<sold>0</sold>
			</item>';
    	}
    	$xml .= '</verkoop>';

    	$response = new Response();
    	$response->headers->set('Content-Type', 'text/xml');
        $response->setContent($xml);
        return $response;
    }
}