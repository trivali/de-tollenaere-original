<?php

namespace Drupal\detollenaere_bandzaagformulier\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
/**
 * Our custom hero form.
 */
class OrderForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return "detollenaere_bandzaagformulier_order_form";
  }

  private $csv_results = null;

  private function read_csv(){

    if ($this->csv_results == null) {
      $node = \Drupal::routeMatch()->getParameter('node');
      if ($node instanceof \Drupal\node\NodeInterface) {
        $config = \Drupal::config('detollenaere_bandzaagformulier.settings');
        if ($config->get('csv_path') != null) {
          $csv_path = $config->get('csv_path');
          $csv_cat = $node->field_excel_categorie->value;

          $results = [];
          $file = fopen($csv_path, 'r');
          while (($line = fgetcsv($file, 1000, ';')) !== FALSE) {
            if ($line[0] == $csv_cat) {
              $results[] = $line;
            }
          }
          fclose($file);

          $this->csv_results = $results;
        }
      }
    }

    return $this->csv_results;
  }

  private function get_form_options(){

    if ($this->read_csv() != null) {
      $results = $this->read_csv();

      $sizes = $this->clear_options_arr(array_column($results, 1));
      $toothings = $this->clear_options_arr(array_column($results, 2));
      $lengths = $this->clear_options_arr(array_column($results, 3));

      return compact('sizes', 'toothings', 'lengths');
    }

    return null;
  }

  private function clear_options_arr($arr){
    $arr = array_filter(array_unique($arr));
    asort($arr);
    $arr = array_combine($arr, $arr);

    return $arr;
  }

  private function get_size_options($size_val){
    if ($this->read_csv() != null) {
      $results = $this->read_csv();

      $toothings = [];
      $lengths = [];

      foreach ($results as $result) {
        if ($result[1] == $size_val) {
          $toothings[] = $result[2];
        }
      }

      $toothings = $this->clear_options_arr($toothings);
      $first_toothing = array_values($toothings)[0];
      foreach ($results as $result) {
        if ($result[1] == $size_val && $result[2] == $first_toothing) {
          $lengths[] = $result[3];
        }
      }

      $lengths = $this->clear_options_arr($lengths);

      return compact('toothings', 'lengths');
    }

    return null;
  }

  private function get_selected_option($size_val, $toothing_val, $length_val){
    if ($this->read_csv() != null) {
      $results = $this->read_csv();

      foreach ($results as $result) {
        if ($result[1] == $size_val && $result[2] == $toothing_val && $result[3] == $length_val) {
          return $result;
        }
      }
    }

    return null;
  }

  private function get_toothing_options($size_val, $toothing_val){
    if ($this->read_csv() != null) {
      $results = $this->read_csv();

      $lengths = [];

      foreach ($results as $result) {
        if ($result[1] == $size_val && $result[2] == $toothing_val) {
          $lengths[] = $result[3];
        }
      }

      $lengths = $this->clear_options_arr($lengths);

      return compact('lengths');
    }

    return null;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form_state->setCached(FALSE);
    $form['#prefix'] = '<div id="order_form_wrapper" class="container"><div class="row">';
    $form['#suffix'] = '</div></div>';

    $form_options = $this->get_form_options();

    $selected_size = $form_state->getValue('size');
    if (!isset($selected_size)) {
      $selected_size = array_values($form_options['sizes'])[0];
    }

    $other_options = $this->get_size_options($selected_size);
    $form_options['toothings'] = $other_options['toothings'];

    $selected_toothing = $form_state->getValue('toothing');
    if (!isset($selected_toothing) || !in_array($selected_toothing, $form_options['toothings'])) {
      $selected_toothing = array_values($form_options['toothings'])[0];
    }

    $other_options = $this->get_toothing_options($selected_size, $selected_toothing);
    $form_options['lengths'] = $other_options['lengths'];

    $selected_length = $form_state->getValue('length');
    if (!isset($selected_length) || !in_array($selected_length, $form_options['lengths'])) {
      $selected_length = array_values($form_options['lengths'])[0];
    }

    $selected = $this->get_selected_option($selected_size, $selected_toothing, $selected_length);

    $selected_amount = $form_state->getValue('amount');
    if (!isset($selected_amount)) {
      $selected_amount = 1;
    }

    $form['group1'] = array(
      '#type' => 'container',
      '#attributes' => [
        'class' => 'product-order',
      ]
    );

    $form['group1']['message'] = [
      '#type' => 'markup',
      '#markup' => '<div class="text-formatted"><h3>'.t('Order product').'</h3></div>'
    ];

    $form['group1']['size'] = array(
      '#type' => 'select',
      '#title' => t('Height x Width'),
      '#ajax' => [
        'callback' => '::update_options',
        'event' => 'change',
        'wrapper' => 'order_form_wrapper',
        'progress' => [
          'type' => 'throbber',
          'message' => t('Updating attributes...'),
        ],
      ],
      '#options' => $form_options['sizes'],
      '#default_value' => $form_defaults[1]
    );

    $form['group1']['toothing'] = array(
      '#type' => 'select',
      '#title' => t('Toothing'),
      '#ajax' => [
        'callback' => '::update_options',
        'event' => 'change',
        'wrapper' => 'order_form_wrapper',
        'progress' => [
          'type' => 'throbber',
          'message' => t('Updating attributes...'),
        ],
      ],
      '#options' => $form_options['toothings'],
      '#default_value' => array_values($other_options['toothings'])[0]
    );

    $form['group1']['length'] = array(
      '#type' => 'select',
      '#title' => t('Length'),
      '#ajax' => [
        'callback' => '::update_options',
        'event' => 'change',
        'wrapper' => 'order_form_wrapper',
        'progress' => [
          'type' => 'throbber',
          'message' => t('Updating attributes...'),
        ],
      ],
      '#options' => $form_options['lengths'],
      '#default_value' => array_values($other_options['lengths'])[0]
    );

    $form['pricing'] = array(
      '#type' => 'container',
      '#attributes' => [
        'class' => 'product-pricing',
      ]
    );

     $form['pricing']['price'] = [
       '#type' => 'inline_template',
       '#template' => '<div class="item_price"><label>'.t('Price').'</label><span>{{ somecontent }}</span></div>',
       '#context' => [
          'somecontent' => $selected[4]
        ]
     ];

    $form['pricing']['amount'] = array(
      '#type' => 'number',
      '#title' => t('Amount'),
      '#ajax' => [
        'callback' => '::update_options',
        'event' => 'change',
        'wrapper' => 'order_form_wrapper',
      ],
    );

    $price = (float)str_replace(',','.',str_replace(' ', '', str_replace('€','', $selected[4])));
    $total = $price * $selected_amount;

    $form['pricing']['price_total'] = [
       '#type' => 'inline_template',
       '#template' => '<div class="item_price"><label>'.t('Total price').'</label><span>{{ somecontent }}</span></div>',
       '#context' => [
          'somecontent' => '€ '.str_replace('.',',', $total)
        ]
    ];

    $form['actions'] = array(
      '#type' => 'container',
      '#attributes' => [
        'class' => 'product-actions',
      ]
    );

    $form['actions']['modal_button'] = [
       '#type' => 'inline_template',
       '#template' => '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#orderModal">'.t('Place order').'</button>',
     ];

    $form['group2']['modal_start'] = [
       '#type' => 'inline_template',
       '#template' => '<div class="modal fade" id="orderModal" role="dialog" aria-labelledby="orderModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="orderModalLabel">{{modal_title}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"><div class="item-info">
      <h6>{{item_category}}</h6>
      <table class="table table-sm">
      <thead>
        <tr>
          <th>'.t('Size').'</th>
          <th>'.t('Toothing').'</th>
          <th>'.t('Length').'</th>
          <th>'.t('Price').'</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>{{item_size}}</td>
          <td>{{item_toothing}}</td>
          <td>{{item_length}}</td>
          <td>{{item_price}}</td>
        </tr>
      </tbody>
      <tfoot>
        <tr>
          <td colspan="2"></td>
          <td>'.t('Amount').'</td>
          <td>{{item_amount}}</td>
        </tr>
      </tfoot>
      </table>
    </div>',
      '#context' => [
          'modal_title' => t('Order product'),
          'item_category' => $selected[0],
          'item_size' => $selected[1],
          'item_toothing' => $selected[2],
          'item_length' => $selected[3],
          'item_price' => $selected[4],
          'item_amount' => $selected_amount,
        ]
     ];

    $form['group2']['company'] = array(
      '#type' => 'textfield',
      '#title' => t('Company'),
    );

    $form['group2']['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Name'),
    );

    $form['group2']['address'] = array(
      '#type' => 'textfield',
      '#title' => t('Address'),
    );

    $form['group2']['vat'] = array(
      '#type' => 'textfield',
      '#title' => t('VAT'),
    );

    $form['group2']['email'] = array(
      '#type' => 'email',
      '#title' => t('Email'),
    );

    $form['group2']['phone'] = array(
      '#type' => 'textfield',
      '#title' => t('Phone'),
    );

    $form['group2']['modal_footer_start'] = [
       '#type' => 'inline_template',
       '#template' => '</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">'.t('Cancel').'</button>
        ',
     ];

    $form['group2']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Finish order'),
      '#button_type' => 'primary',
    );

    $form['group2']['modal_footer_end'] = [
       '#type' => 'inline_template',
       '#template' => '
      </div>
    </div>
  </div>
</div>',
     ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $selected_size = $form_state->getValue('size');
    $selected_toothing = $form_state->getValue('toothing');
    $selected_length = $form_state->getValue('length');
    $selected_amount = $form_state->getValue('amount');
    $selected = $this->get_selected_option($selected_size, $selected_toothing, $selected_length);

    $message = '
    <h1>Nieuwe bestelling bandzaagmachine</h1>
    <table width="500">
      <tbody>
        <tr>
          <td>'.t('Company').'</td>
          <td>'.$form_state->getValue('company').'</td>
          <td>'.t('Name').'</td>
          <td>'.$form_state->getValue('name').'</td>
        </tr>
         <tr>
          <td>'.t('Address').'</td>
          <td>'.$form_state->getValue('address').'</td>
          <td>'.t('VAT').'</td>
          <td>'.$form_state->getValue('vat').'</td>
        </tr>
        <tr>
          <td>'.t('Email').'</td>
          <td>'.$form_state->getValue('email').'</td>
          <td>'.t('Phone').'</td>
          <td>'.$form_state->getValue('phone').'</td>
        </tr>
      </tbody>
    </table>
    <br><br>
    <table width="500">
      <thead>
        <tr>
          <th align="left">'.t('Size').'</th>
          <th align="left">'.t('Toothing').'</th>
          <th align="left">'.t('Length').'</th>
          <th align="right">'.t('Price').'</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>'.$selected_size.'</td>
          <td>'.$selected_toothing.'</td>
          <td>'.$selected_length.'</td>
          <td align="right">'.$selected[4].'</td>
        </tr>
      </tbody>
      <tfoot>
        <tr>
          <td colspan="2"></td>
          <td>'.t('Amount').'</td>
          <td align="right">'.$selected_amount.'</td>
        </tr>
      </tfoot>
      </table>
    ';

    $from = "info@detollenaere.eu";
    $to = "info@detollenaere.eu";
    $subject = "Nieuwe bestelling bandzaagmachine";
    $headers = "From: info@detollenaere.eu\r\n";
    $headers .= "Reply-To: ".$form_state->getValue('email')."\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
    mail($to,$subject,$message, $headers);

    $form_state->setRedirect('entity.node.canonical', array('node' => 564));

    return;
  }

  public function update_options(array &$form, FormStateInterface $form_state) {

    $form_state->setRebuild();

    return $form;

  }

}
