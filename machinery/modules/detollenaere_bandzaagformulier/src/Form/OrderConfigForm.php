<?php

namespace Drupal\detollenaere_bandzaagformulier\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;

/**
 * Our config form.
 */
class OrderConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return "detollenaere_bandzaagformulier_settings";
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('detollenaere_bandzaagformulier.settings');

    $form['csv'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('CSV-bestand'),
      '#upload_location' => 'private://detollenaere',
      '#upload_validators' => [
        'file_validate_extensions' => ['csv'],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'detollenaere_bandzaagformulier.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('detollenaere_bandzaagformulier.settings');

    $form_file = $form_state->getValue('csv', 0);
    if (isset($form_file[0]) && !empty($form_file[0])) {
      $file = File::load($form_file[0]);
      $file->setPermanent();
      $file->save();
      $uri = $file->getFileUri();
      $stream_wrapper_manager = \Drupal::service('stream_wrapper_manager')->getViaUri($uri);
      $file_path = $stream_wrapper_manager->realpath();
    }

    $config->set('csv_path', $file_path)->save();

    parent::submitForm($form, $form_state);
  }
}
