<?php

namespace Drupal\detollenaere_bandzaagformulier\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a block called "Saw order form".
 *
 * @Block(
 *  id = "detollenaere_bandzaagformulier_order_block",
 *  admin_label = @Translation("Saw order form")
 * )
 */
class OrderBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = \Drupal::formBuilder()->getForm('Drupal\detollenaere_bandzaagformulier\Form\OrderForm');
    return $form;
  }
}
