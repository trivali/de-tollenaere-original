/**
 * @file
 * Global utilities.
 *
 */
(function ($) {
	// Bust IE11 cache
	$.ajaxSetup({cache: false});

	//Check popup
	$.get('/cookies.php', function(data){
		var result = JSON.parse(data);
		if (result) {
			$('.initial-popup').show();
		}
	});

	$('.home-slider-top .view-content .home-slider-wrapper').unslider({
		arrows: {
			//  Unslider default behaviour
			prev: '<a class="unslider-arrow prev"><span>&#139;</span></a>',
			next: '<a class="unslider-arrow next"><span>&#155;</span></a>',
		}
	});

	if ($('ul.unslider-carousel li').length <= 1) {
		$('.unslider .unslider-arrow, .unslider .unslider-nav').hide();
	}

	$(document).on('click', 'a[href^="#"]', function (event) {
		event.preventDefault();

		if ($(this).attr('data-nobookmark-mobile') == 'true' && $(window).outerWidth() <= 767) {
			// do nothing
		} else {
			$('html, body').animate({
				scrollTop: $($.attr(this, 'href')).offset().top - 100
			}, 500);
		}
	});

	$('.path-frontpage #block-menutop, .page-node-481 #block-menutop').addClass('animated bounceInDown');

	$('.initial-popup').click(function(){
		$(this).remove();
	});

	$('[data-popup-action="close"]').click(function(){
		$('.initial-popup').remove();
	});

	$('.view-in-de-kijker .view-content').slick({
		slidesToShow: 3,
		responsive: [
			{
				breakpoint: 768,
				settings: {
					arrows: false,
					slidesToShow: 2
				}
			},
			{
				breakpoint: 480,
				settings: {
					arrows: false,
					slidesToShow: 1
				}
			}
		]
	});

	$('#edit-field-regio, #edit-field-regio--2').each(function(i, el){
		$(el).parent().parent().css('overflow', 'hidden');
		$(el).parent().parent().css('margin', '0');
		$(el).change(function(){
			var tax_id = $(this).val();
			var url = '/api/taxonomy/term/'+tax_id+'?_format=json';
			if (tax_id == '_none') {
				url = '/api/vertegenwoordiger/521?_format=json';
			}
			$select = $(this);
			$.ajax({
				 async: true,   // this will solve the problem
				 type: "GET",
				 url: url,
				 contentType: "application/json",
			}).done(function(data){
				change_region_email($select, data);
			});
		});
	});

	$.ajax({
		 async: true,   // this will solve the problem
		 type: "GET",
		 url: '/api/vertegenwoordiger/521?_format=json',
		 contentType: "application/json",
	}).done(function(data){
		$('#edit-field-regio').val('_none');
		$('#edit-field-regio--2').val('_none');
		change_region_email($('#edit-field-regio'), data);
		change_region_email($('#edit-field-regio--2'), data);
	});

	$('#edit-field-e-mailadre-0-value, #edit-field-e-mailadre-0-value--2').attr('autocomplete', 'off');

	function change_region_email($select, data){
		if (data.length > 0) {
				data = data[0];
				var name = data.title[0].value;
			var telephone = data.field_telefoon[0].value;
			var email = data.field_e_mailadrs[0].value;
			if (data.field_image.length > 0) {
				var image = data.field_image[0];
			}else{
				var image = {url:""};
			}

			$('#edit-field-e-mailadre-0-value, #edit-field-e-mailadre-0-value--2', $select.parent().parent().parent()).val(email);

			$('+ .region-representative', $select.parent()).remove();
			$select.parent().after('<div class="region-representative" style="clear: both;width:100%;padding: 0 15px;"><div style="color:#000000;background-color:#eeeeee;padding:15px;margin-bottom:15px;"><div class="image" style="width:28%;float:left;margin-right:10px;"><img class="img-fluid" src="'+image.url+'" /></div><div class="info" style="text-align:left;font-size:.9em;">'+name+'<br /><a href="mailto:'+email+'">'+email+'</a><br /><a href="tel:'+telephone+'">'+telephone+'</a></div><div style="clear:both;"></div></div></div>');
		}else{
			$('#edit-field-e-mailadre-0-value, #edit-field-e-mailadre-0-value--2', $select.parent().parent().parent()).val('marc@detollenaere.be');
		}
	}

	$('#edit-field-type').each(function(i, el){
		$('.type-representative').remove();
		$type_select = $(this);
		 $.ajax({
				 async: true,   // this will solve the problem
				 type: "GET",
				 url: '/api/vertegenwoordiger/538?_format=json',
				 contentType: "application/json",
			}).done(function(data){
				if (data.length > 0) {
					data = data[0];

					var name = data.title[0].value;
					var telephone = data.field_telefoon[0].value;
					var email = data.field_e_mailadrs[0].value;
					if (data.field_image.length > 0) {
						var image = data.field_image[0];
					}else{
						var image = {url:""};
					}

					$type_select.parent().after('<div class="type-representative" style="clear: both;width:100%;padding: 0 15px;"><div style="color:#000000;background-color:#eeeeee;padding:15px;margin-bottom:15px;"><div class="image" style="width:28%;float:left;margin-right:10px;"><img class="img-fluid" src="'+image.url+'" /></div><div class="info" style="text-align:left;font-size:.9em;">'+name+'<br /><a href="mailto:'+email+'">'+email+'</a><br /><a href="tel:'+telephone+'">'+telephone+'</a></div><div style="clear:both;"></div></div></div>')
				}
			});
	});

	$('.form-item-field-regio option[value="_none"]').text(Drupal.t('Selecteer een regio'));
	$('.form-item-field-type option[value="_none"]').text(Drupal.t('Selecteer een type'));

	$('.view-getuigenissen .view-content').slick({
		slidesToShow: 1,
		arrows: true,
		autoplay: false
	});

	$(window).resize(function(){
		if ($(window).width() < 768 && !$('.view-laatste-nieuws-en-tips .view-content').hasClass('slick-initialized')){
			$('.view-laatste-nieuws-en-tips .view-content').slick({
				slidesToShow: 1,
				arrows: false,
				autoplay: false,
				dots: true
			});
		}else if($(window).width() >= 768 && $('.view-laatste-nieuws-en-tips .view-content').hasClass('slick-initialized')){
			$('.view-laatste-nieuws-en-tips .view-content').slick('unslick');
		}
	});
	$(window).resize();

	$('.view-reviews .view-content').slick({
		slidesToShow: 1,
		arrows: true,
		autoplay: false
	});

	$('.shs-container .form-select').customSelect();

	$('.form-group').each(function(index, element){
		$label = $(element).find('label');
		var id = $label.attr('for');
		$('#'+id).attr('placeholder', $label.text());
	});

	$('.file--application-pdf a, .field--name-field-website-merk a').attr('target', '_blank');

	$tabs = $('.product-tabs');

	if ($tabs.length > 0) {

		// desktop
		if ($(window).outerWidth() > 767) {
			$tabs.width($tabs.outerWidth());
			$('.product-tabs-placeholder').height($tabs.outerHeight());
			var startY = $tabs.offset().top;
			var endY = $('.product-info-wrapper').offset().top - $('.product-info-wrapper').outerHeight()-$tabs.outerHeight();

			setInterval(function(){
				var top = $(window).scrollTop();

				if (top > startY && top < endY) {
					$tabs.addClass('fixed');
				} else{
					$tabs.removeClass('fixed');
				}

				top = top+60;
				$tabs.find('a').removeClass('active');
				$tabs.find('a:first').addClass('active');
				$tabs.find('a').each(function(i, el){
					var id = $(el).attr('href');
					var hash_top = $(id).offset().top;

					if (hash_top <= top && hash_top+$(id).outerHeight() > top) {
						$tabs.find('a:first').removeClass('active');
						$(el).addClass('active');
						return false;
					}
				});
			}, 20);

		// mobile
		} else {
			$('.product-info-wrapper .tab-pane').hide();
			$('.product-info-wrapper .tab-pane').first().show();
			$('.product-tabs a').removeClass('active');
			$('.product-tabs a').first().addClass('active');

			$('.product-tabs a').click(function(e) {
				e.preventDefault();
				var href = $(this).attr('href');

				$('.product-tabs a').removeClass('active');
				$(this).addClass('active');

				$('.product-info-wrapper .tab-pane').hide();
				$(href).show();
			});
		}
	}

	$cta_left = $('.cta-left-fixed');

	if ($cta_left.length > 0) {
		$cta_left.css('width', $cta_left.parent().width()+'px');
		$('.cta-left-fixed-placeholder').height($cta_left.outerHeight());

		var startY_cta = $cta_left.offset().top;
		var endY_cta = $('.product-info-wrapper').offset().top + $('.product-info-wrapper').outerHeight()-$cta_left.outerHeight();

		setInterval(function(){
			var top = $(window).scrollTop()+15;
			if (top > startY_cta && top < endY_cta) {
				$cta_left.addClass('fixed');
			} else{
				$cta_left.removeClass('fixed');
			}
		}, 20);
	}

})(jQuery);
